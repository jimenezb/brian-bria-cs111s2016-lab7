//*****************************
//Name: Brian Jimenez Bria Dorsainvil
//Class: CS111S2016
//Lab 7
//Honor Code:
//Purpose: To fix bugs within a program provided
//*****************************
import java.util.Date;
import java.util.Scanner;
import java.lang.Math;

public class CommandLineGeometer {

    private enum GeometricShape {sphere, triangle, cylinder};

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);		//Needed for use input
        GeometricShape shape = GeometricShape.sphere;	//We will be working with the sphere formula
        double radius;					//radius of sphere/cylinder has value double
        double height;					//height of cylinder has value double
        double x;					//side 1 of triangle size
        double y;					//side 2 of triangle size
        double z;					//side 3 of triangle size

        System.out.println("Brian Jimenez, Bria Dorsainvil " + new Date()); //print out creator's name
        System.out.println("Welcome to the Command Line Geometer!"); //welcomes user
        System.out.println();

        System.out.println("What is the radius for the " + shape + "?"); //asks for radius value
        radius = scan.nextDouble(); //assigns the value inputed to variable radius
        System.out.println();

        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius); //says that we will calculate volume of a sphere using formula in Geometric calculator
        double sphereVolume = GeometricCalculator.calculateSphereVolume(radius); //calculates volume of a sphere using formula in Geometric calculator
        System.out.println("The volume is equal to " + sphereVolume); //gives output for our calculation
        System.out.println();

        shape = GeometricShape.triangle; //new shape is our triangle

        System.out.println("What is the length of the first side?"); 	//asks user for length
        x = scan.nextDouble();						//assigns length to side 1

        System.out.println("What is the length of the second side?");	 //asks user for length
        y = scan.nextDouble();						//assigns length to side 2

        System.out.println("What is the length of the third side?"); 	//asks user for length
        z = scan.nextDouble();						//assigns length to side 3
        System.out.println();

        System.out.println("Calculating the area of a " + shape);			//says we will be calculating area of a triangle
        double triangleArea = GeometricCalculator.calculateTriangleArea(x, y, x);	//calculates area of a triangle using the formula from Geometric calculator
        System.out.println("The area is equal to " + triangleArea);			//prints the result from area of the triangle
        System.out.println();

        shape = GeometricShape.cylinder;						//new shape is our cylinder

        System.out.println("What is the radius for the " + shape + "?");		//asks for a radius for our cylinder
        radius = scan.nextDouble();							//asigns radius that was inputed by user
        System.out.println();

        System.out.println("What is the height for the " + shape + "?");		//asks for height for our cylinder
        height = scan.nextDouble();							//assigns height that was inputed by user
        System.out.println();

        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);		//says it will calculate volume of the cylinder
        double cylinderVolume = GeometricCalculator.calculateCylinderVolume(radius, height);			//calculates volume of the cylinder
        System.out.println("The volume is equal to " + cylinderVolume);						//prints the result from the volume of our cylinder
        System.out.println();
    }
}

public class GeometricCalculator {

    public static double calculateSphereVolume(double radius)		// double needed to use this formula
    {
        double volume;							// makes variable volume
        volume = (4.00/3.00) * (Math.PI) * Math.pow(radius,3.00);	// assigns equation for volume sphere
        return volume;							// returns a value
    }

    public static double calculateTriangleArea(double x, double y, double z)	//x,y,z doubles are needed to use this formula
    {
        double area;								// makes variable for the area
	double p = 0.5 *( x + y + z);						// makes variable to use in area equation
	double k = p*(p-x)*(p-y)*(p-z);						// makes variable to define the variable equation
        area = Math.pow(k,0.5);							// finishes calculations and gives official area of triangle when only three sides are given
        return area;								// returns a value
    }

    public static double calculateCylinderVolume(double radius, double height)	//radius double and height double is needed to use this equation
    {
        double volume;								// makes variable for the volume
        volume = (Math.PI) * Math.pow(radius,2.00) * height;			// assigns variable with the formula for the volume of a cylinder
        return volume;								// returns a value
    }
}
